package ro.computas.javasix.bean;

import javax.ejb.Stateless;
import javax.inject.Named;

@Named
@Stateless
public class Simple {
    public String hello(){
        return "Hello";
    }
}
