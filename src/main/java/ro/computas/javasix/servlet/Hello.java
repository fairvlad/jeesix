package ro.computas.javasix.servlet;

import ro.computas.javasix.bean.Simple;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/hello")
public class Hello extends HttpServlet {
    @Inject
    Simple simple;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.println("<html><head>");
        out.println("<title>MyServlet</title>");
        out.println("</head><body>");
        out.println("<h1>"+simple.hello()+"</h1>");
        out.println("</body></html>");
        super.doGet(req, resp);
    }
}
